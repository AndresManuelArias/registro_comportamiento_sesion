﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using proyecto_final.Models;
using System.Reflection;


namespace proyecto_final.Controllers
{

    public class PerfilesController : ControlFather
    {
        public  PropertyInfo[] propertyInfo = typeof(Perfiles).GetProperties();
        public Perfiles modeloAgregarDatos= new Perfiles();
        public  dynamic modeloObjetoCrud  = _registro_comportamiento_estudiantesContext.Perfiles;
        public override dynamic modelos(){
            return _registro_comportamiento_estudiantesContext.Perfiles.ToList();
        }
        public override configuracion modelo(){ 
            configuracion _configuracion = new configuracion{
                propertyInfo = propertyInfo,
                modeloAgregarDatos = modeloAgregarDatos,
                modeloObjetoCrud = modeloObjetoCrud,
            };
            return _configuracion;
        }
    }
}
