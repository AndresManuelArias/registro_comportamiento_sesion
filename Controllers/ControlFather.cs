using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using proyecto_final.Models;
using System.Reflection;


namespace proyecto_final.Controllers
{

    public abstract class ControlFather : Controller
    {

        public struct configuracion{
             public  PropertyInfo[] propertyInfo;
            public dynamic modeloAgregarDatos;
            public dynamic modeloObjetoCrud;
        }
        static public registro_comportamiento_estudiantesContext _registro_comportamiento_estudiantesContext = new registro_comportamiento_estudiantesContext();
        public abstract dynamic  modelos(); 
        public abstract configuracion  modelo(); 
        // public abstract PropertyInfo  objeto(); 
        public ActionResult Index()
        {
            return View(modelos());
        }
        public ActionResult insert()
        {
            return View();
        }

        [HttpPost] 
        public IActionResult  insert(int? number)
        {                    
            for(int campo = 1;campo < modelo().propertyInfo.Length;campo++){                                 
               dynamic datoConvertido = Convert.ChangeType(Request.Form[modelo().propertyInfo[campo].Name].ToString(), modelo().propertyInfo[campo].PropertyType);
               modelo().propertyInfo[campo].SetValue(modelo().modeloAgregarDatos, datoConvertido);             
            }
            modelo().modeloObjetoCrud.Add(modelo().modeloAgregarDatos);
            _registro_comportamiento_estudiantesContext.SaveChanges();
            return RedirectToAction("Index");  
        }
        private object buscar(int id){
            dynamic[] objetoBuscar = modelos().ToArray();
            object objetoEncontrado= new object();
            for(int dato = 0; dato < objetoBuscar.Length ;dato++){
                if(int.Parse(modelo().propertyInfo[0].GetValue(objetoBuscar[dato]).ToString()) == id){
                    objetoEncontrado = objetoBuscar[dato];
                    break;
                }                    
            }
            return objetoEncontrado;
        } 
        public IActionResult Edit(int id)
        {
            try{
                object objetoEncontrado =  buscar(id);
                return View(objetoEncontrado);
            }catch{
                return View();
            }
        }
        [HttpPost]
        public ActionResult Edit()
        {
            try{
                int id = Int32.Parse(Request.Form[modelo().propertyInfo[0].Name].ToString());
                object existingUsuario =  buscar(id);
                for(int campo = 0;campo < modelo().propertyInfo.Length;campo++){                                 
                    dynamic datoConvertido = Convert.ChangeType(Request.Form[modelo().propertyInfo[campo].Name].ToString(), modelo().propertyInfo[campo].PropertyType);
                    modelo().propertyInfo[campo].SetValue(existingUsuario, datoConvertido);             
                }
                _registro_comportamiento_estudiantesContext.Attach(existingUsuario).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _registro_comportamiento_estudiantesContext.SaveChanges();                
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return RedirectToAction("Index");
            }
        }

        public IActionResult Delete(int id)
        {   
            try{
                object objetoEncontrado =  buscar(id);
                return View(objetoEncontrado);
            }catch{
               return View();
            }
        }
        
        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete()
        {
             try{
                int id = Int32.Parse(Request.Form[modelo().propertyInfo[0].Name].ToString());
                dynamic existingUsuario =  buscar(id);    
                _registro_comportamiento_estudiantesContext.Remove(existingUsuario);
                _registro_comportamiento_estudiantesContext.SaveChanges();                                
                return RedirectToAction("Index");
            }catch (Exception e){
                Console.WriteLine(e.Message);
                return RedirectToAction("Index");
            }
        }
        // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            try{
                object existingUsuario =  buscar(id);            
                return View(existingUsuario);
            }catch{
                return View();
            }
        }

    }
}