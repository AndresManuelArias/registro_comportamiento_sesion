﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using proyecto_final.Models;
using System.Reflection;


namespace proyecto_final.Controllers
{

    public class UsuariosController : ControlFather
    {
        public  PropertyInfo[] propertyInfo = typeof(Usuarios).GetProperties();
        public Usuarios modeloAgregarDatos= new Usuarios();
        public  dynamic modeloObjetoCrud  = _registro_comportamiento_estudiantesContext.Usuarios;
        public override dynamic modelos(){
            return _registro_comportamiento_estudiantesContext.Usuarios.ToList();
        }
        public override configuracion modelo(){ 
            configuracion _configuracion = new configuracion{
                propertyInfo = propertyInfo,
                modeloAgregarDatos = modeloAgregarDatos,
                modeloObjetoCrud = modeloObjetoCrud,
            };
            return _configuracion;
        }
    }
}
