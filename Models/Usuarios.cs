﻿using System;
using System.Collections.Generic;

namespace proyecto_final
{
    public partial class Usuarios
    {
        public int IdUsuario { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
        public long NumeroIdentificacion { get; set; }
        public string Nombre { get; set; }
    }
}
