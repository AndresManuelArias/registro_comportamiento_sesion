﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace proyecto_final
{
    public partial class registro_comportamiento_estudiantesContext : DbContext
    {
        public registro_comportamiento_estudiantesContext()
        {
        }

        public registro_comportamiento_estudiantesContext(DbContextOptions<registro_comportamiento_estudiantesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Perfiles> Perfiles { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;database=registro_comportamiento_estudiantes;user=root;password=NuevoMundo%1;treattinyasboolean=true", x => x.ServerVersion("10.3.20-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Perfiles>(entity =>
            {
                entity.HasKey(e => e.Perfiles1)
                    .HasName("PRIMARY");

                entity.ToTable("perfiles");

                entity.HasIndex(e => e.Nombre)
                    .HasName("usuario")
                    .IsUnique();

                entity.Property(e => e.Perfiles1)
                    .HasColumnName("perfiles")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Usuarios>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PRIMARY");

                entity.ToTable("usuarios");

                entity.HasIndex(e => new { e.Nickname, e.NumeroIdentificacion })
                    .HasName("usuario")
                    .IsUnique();

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("id_usuario")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Nickname)
                    .HasColumnName("nickname")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.NumeroIdentificacion)
                    .HasColumnName("numero identificacion")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
