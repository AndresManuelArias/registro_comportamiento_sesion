-- aqui se crea el codigo para la base de datos
create database registro_comportamiento_estudiantes;
use registro_comportamiento_estudiantes;

create table if not exists usuarios(
	id_usuario int NOT NULL AUTO_INCREMENT,
    nickname varchar(50),
    password varchar(50),
    `numero identificacion` BIGINT,
    nombre varchar(200),
     PRIMARY KEY (id_usuario),
    CONSTRAINT usuario UNIQUE (nickname, `numero identificacion`)
);

create table if not exists perfiles(
	perfiles int NOT NULL AUTO_INCREMENT,
    nombre varchar(50),
     PRIMARY KEY (perfiles),
    CONSTRAINT usuario UNIQUE (nombre)
);



